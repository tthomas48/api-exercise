# Atlassian API Exercise

## Execution

A test API server can be started by compiling the project and then executing the resulting jar:

  mvn install
  java -jar target/api-exercise-1.0-SNAPSHOT.jar server
  
The client can be run against the server (or any server) with the following command:

  java -cp target/api-exercise-1.0-SNAPSHOT.jar org.bitbucket.tthomas48.api.exercise.ApiClient http://localhost:8080/ bug,task,story 

If you don't want to specify command line arguments you can configure default settings in api-client.properties.
