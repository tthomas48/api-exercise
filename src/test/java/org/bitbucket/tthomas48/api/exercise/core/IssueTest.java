package org.bitbucket.tthomas48.api.exercise.core;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;

import org.bitbucket.tthomas48.api.exercise.core.Issue;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class IssueTest {
	private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

	@Test
	public void serializesToJSON() throws Exception {
		// #1
		Issue issue = new Issue("/issues/1", "/issuetypes/bug", "Issue #1", 3);

		String expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/issue-1.json"), Issue.class));

		assertThat(MAPPER.writeValueAsString(issue)).isEqualTo(expected);

		// #2
		issue = new Issue("/issues/2", "/issuetypes/bug", "Issue #2", 5);

		expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/issue-2.json"), Issue.class));

		assertThat(MAPPER.writeValueAsString(issue)).isEqualTo(expected);
		
		// #3
		issue = new Issue("/issues/3", "/issuetypes/bug", "Issue #3", 2);

		expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/issue-3.json"), Issue.class));

		assertThat(MAPPER.writeValueAsString(issue)).isEqualTo(expected);		
		
		// #4
		issue = new Issue("/issues/4", "/issuetypes/story", "Issue #4", 3);

		expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/issue-4.json"), Issue.class));

		assertThat(MAPPER.writeValueAsString(issue)).isEqualTo(expected);
		
		// #4
		issue = new Issue("/issues/5", "/issuetypes/story", "Issue #5", 1);

		expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/issue-5.json"), Issue.class));

		assertThat(MAPPER.writeValueAsString(issue)).isEqualTo(expected);		
		
	}

}
