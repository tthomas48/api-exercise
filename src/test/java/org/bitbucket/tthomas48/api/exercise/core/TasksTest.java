package org.bitbucket.tthomas48.api.exercise.core;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TasksTest {
	private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

	@Test
	public void serializesToJSON() throws Exception {
		Tasks tasks = new Tasks("/issuetypes/task", "task", new String[] {
		});

		String expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/tasks.json"), Tasks.class));

		assertThat(MAPPER.writeValueAsString(tasks)).isEqualTo(expected);

	}

}
