package org.bitbucket.tthomas48.api.exercise.core;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;

import org.bitbucket.tthomas48.api.exercise.core.Bugs;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class StoriesTest {
	private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

	@Test
	public void serializesToJSON() throws Exception {
		Stories stories = new Stories("/issuetypes/story", "story",
				new String[] { "/issues/4", "/issues/5" });

		String expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/stories.json"), Stories.class));

		assertThat(MAPPER.writeValueAsString(stories)).isEqualTo(expected);

	}

}
