package org.bitbucket.tthomas48.api.exercise.core;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;

import org.bitbucket.tthomas48.api.exercise.core.Bugs;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BugsTest {
	private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

	@Test
	public void serializesToJSON() throws Exception {
		Bugs bugs = new Bugs("/issuetypes/bug", "bug", new String[] {
				"/issues/1",
				"/issues/2",
				"/issues/3"
		});

		String expected = MAPPER.writeValueAsString(MAPPER.readValue(
				fixture("fixtures/bugs.json"), Bugs.class));

		assertThat(MAPPER.writeValueAsString(bugs)).isEqualTo(expected);

	}

}
