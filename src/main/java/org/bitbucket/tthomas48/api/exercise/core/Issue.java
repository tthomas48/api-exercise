package org.bitbucket.tthomas48.api.exercise.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Issue {

	private String id;
	private String issuetype;
	private String description;
	private long estimate;
	
	public Issue() {
		
	}

	public Issue(String id, String issuetype, String description, long estimate) {
		this.id = id;
		this.issuetype = issuetype;
		this.description = description;
		this.estimate = estimate;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	@JsonProperty
	public String getIssuetype() {
		return issuetype;
	}

	@JsonProperty
	public String getDescription() {
		return description;
	}

	@JsonProperty
	public long getEstimate() {
		return estimate;
	}

}
