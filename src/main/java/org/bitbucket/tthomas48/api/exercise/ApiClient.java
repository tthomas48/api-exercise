package org.bitbucket.tthomas48.api.exercise;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.bitbucket.tthomas48.api.exercise.core.IHaveIssues;
import org.bitbucket.tthomas48.api.exercise.core.Issue;

public class ApiClient {

	private static final ExecutorService executorService = Executors
			.newFixedThreadPool(5);
	private String host;
	private ValidTypes[] types;

	public ApiClient() {

	}

	public static void main(String[] args) throws Exception {
		ApiClient client = new ApiClient();
		client.loadProperties();
		client.parseArgs(args);
		if (StringUtils.isEmpty(client.host) || client.types == null) {
			client.printUsage();
			return;
		}

		client.generateSummary();
	}

	private void loadProperties() throws IOException {
		Properties properties = new Properties();
		File propertiesFile = new File("api-client.properties");
		if (!propertiesFile.exists()) {
			return;
		}
		FileInputStream input = new FileInputStream(propertiesFile);
		try {
			properties.load(input);
		} finally {
			input.close();
		}

		if (properties.containsKey("host")) {
			host = (String) properties.get("host");
		}

		if (properties.containsKey("type")) {
			parseTypes((String) properties.get("type"));
		}

	}

	private void parseArgs(String[] args) {

		if (args.length > 0) {
			host = args[0];
		}

		if (args.length > 1) {
			parseTypes(args[1]);
		}
	}

	private void parseTypes(String typeSetting) throws IllegalArgumentException {
		String[] typeStrings = typeSetting.toUpperCase().split(",");
		types = new ValidTypes[typeStrings.length];
		for (int i = 0; i < typeStrings.length; i++) {
			types[i] = ValidTypes.valueOf(typeStrings[i]);
		}
	}

	private void printUsage() {
		System.err.println("Usage: java -cp api-exercise-1.0-SNAPSHOT.jar "
				+ ApiClient.class.getName() + " [host] ["
				+ StringUtils.join(ValidTypes.values(), ',').toLowerCase()
				+ "]");
	}

	private void generateSummary() {
		final Client client = ClientBuilder.newClient();

		List<Callable<Issue>> httpGets = new ArrayList<Callable<Issue>>();
		for (ValidTypes type : types) {
			WebTarget target = client.target(host).path(
					"/issuetypes/" + type.toString().toLowerCase());
			IHaveIssues haveIssues = target.request(
					MediaType.APPLICATION_JSON_TYPE).get(type.getModel());

			String[] issueUrls = haveIssues.getIssues();
			for (final String issueUrl : issueUrls) {

				httpGets.add(new Callable<Issue>() {

					public Issue call() throws Exception {
						return client.target(host).path(issueUrl)
								.request(MediaType.APPLICATION_JSON_TYPE)
								.get(Issue.class);
					}
				});
			}
		}

		List<Future<Issue>> issues = null;
		try {
			issues = executorService.invokeAll(httpGets);
		} catch (InterruptedException e) {
			System.err.println("Interrupted while retrieving issues: "
					+ e.getMessage());
		}
		executorService.shutdown();

		Map<String, Long> summary = new HashMap<String, Long>();
		if (issues != null) {
			for (Future<Issue> future : issues) {
				try {
					Issue issue = future.get();
					String type = issue.getIssuetype();

					if (!summary.containsKey(type)) {
						summary.put(type, issue.getEstimate());
						continue;
					}
					summary.put(type,
							summary.get(type) + (long) issue.getEstimate());

				} catch (Exception e) {
					System.err.println("Unable to retrieve issue: "
							+ e.getMessage());
				}
			}
		}
		formatSummary(summary);

	}

	private void formatSummary(Map<String, Long> summary) {

		System.out.printf("%-25s: %5s\n", "Type", "Total");
		for (Entry<String, Long> entry : summary.entrySet()) {
			System.out.printf("%-25s: %5d\n", entry.getKey(), entry.getValue());
		}
	}

}
