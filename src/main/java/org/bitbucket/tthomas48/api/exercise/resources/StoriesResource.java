package org.bitbucket.tthomas48.api.exercise.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bitbucket.tthomas48.api.exercise.core.Stories;

import com.codahale.metrics.annotation.Timed;

@Path("/issuetypes/story")
@Produces(MediaType.APPLICATION_JSON)
public class StoriesResource {

	public StoriesResource() {
	}

	@GET
	@Timed
	public Stories getStories() {
		return new Stories("/issuetypes/story", "story", new String[] {
				"/issues/4", "/issues/5" });
	}
}