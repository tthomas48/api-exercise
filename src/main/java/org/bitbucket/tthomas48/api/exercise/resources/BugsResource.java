package org.bitbucket.tthomas48.api.exercise.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bitbucket.tthomas48.api.exercise.core.Bugs;

import com.codahale.metrics.annotation.Timed;

@Path("/issuetypes/bug")
@Produces(MediaType.APPLICATION_JSON)
public class BugsResource {

    public BugsResource() {
    }

    @GET
    @Timed
    public Bugs getBugs() {
        return new Bugs("/issuetypes/bug", "bug", new String[] {
				"/issues/1",
				"/issues/2",
				"/issues/3"
		});
    }
}