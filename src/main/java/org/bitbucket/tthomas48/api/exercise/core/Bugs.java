package org.bitbucket.tthomas48.api.exercise.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Bugs implements IHaveIssues {
	private String id;
	private String name;
	private String[] issues;

	public Bugs() {

	}

	public Bugs(String id, String name, String[] issues) {
		this.id = id;
		this.name = name;
		this.issues = issues;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	@JsonProperty
	public String[] getIssues() {
		return issues;
	}

}
