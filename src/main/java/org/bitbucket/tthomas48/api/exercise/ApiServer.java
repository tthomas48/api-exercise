package org.bitbucket.tthomas48.api.exercise;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.bitbucket.tthomas48.api.exercise.resources.BugsResource;
import org.bitbucket.tthomas48.api.exercise.resources.IssueResource;
import org.bitbucket.tthomas48.api.exercise.resources.StoriesResource;
import org.bitbucket.tthomas48.api.exercise.resources.TasksResource;

public class ApiServer extends Application<ApiConfiguration> {
	public static void main(String[] args) throws Exception {
		new ApiServer().run(args);
	}

	@Override
	public String getName() {
		return "api-server";
	}

	@Override
	public void initialize(Bootstrap<ApiConfiguration> bootstrap) {
	}

	@Override
	public void run(ApiConfiguration configuration, Environment environment) {

		environment.jersey().register(new IssueResource());
		environment.jersey().register(new BugsResource());
		environment.jersey().register(new StoriesResource());
		environment.jersey().register(new TasksResource());
	}
}
