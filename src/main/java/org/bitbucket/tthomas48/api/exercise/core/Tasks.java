package org.bitbucket.tthomas48.api.exercise.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tasks implements IHaveIssues {
	private String id;
	private String name;
	private String[] issues;

	public Tasks() {

	}

	public Tasks(String id, String name, String[] issues) {
		this.id = id;
		this.name = name;
		this.issues = issues;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	@JsonProperty
	public String[] getIssues() {
		return issues;
	}

}
