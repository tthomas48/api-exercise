package org.bitbucket.tthomas48.api.exercise.core;

public interface IHaveIssues {
	public String[] getIssues();
}
