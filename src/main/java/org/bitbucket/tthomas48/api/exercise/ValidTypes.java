package org.bitbucket.tthomas48.api.exercise;

import org.bitbucket.tthomas48.api.exercise.core.Bugs;
import org.bitbucket.tthomas48.api.exercise.core.IHaveIssues;
import org.bitbucket.tthomas48.api.exercise.core.Stories;
import org.bitbucket.tthomas48.api.exercise.core.Tasks;

public enum ValidTypes {
	BUG(Bugs.class), STORY(Stories.class), TASK(Tasks.class);
	private Class<? extends IHaveIssues> model;

	ValidTypes(Class<? extends IHaveIssues> model) {
		this.model = model;
	}

	public Class<? extends IHaveIssues> getModel() {
		return model;
	}
};

