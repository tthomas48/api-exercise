package org.bitbucket.tthomas48.api.exercise.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bitbucket.tthomas48.api.exercise.core.Tasks;

import com.codahale.metrics.annotation.Timed;

@Path("/issuetypes/task")
@Produces(MediaType.APPLICATION_JSON)
public class TasksResource {

	public TasksResource() {
	}

	@GET
	@Timed
	public Tasks getTasks() {
		return new Tasks("/issuetypes/task", "task", new String[] {});
	}
}