package org.bitbucket.tthomas48.api.exercise.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bitbucket.tthomas48.api.exercise.core.Issue;

import com.codahale.metrics.annotation.Timed;

@Path("/issues")
@Produces(MediaType.APPLICATION_JSON)
public class IssueResource {

	public static final Issue[] issues = new Issue[] {
			new Issue("/issues/1", "/issuetypes/bug", "Issue #1", 3),
			new Issue("/issues/2", "/issuetypes/bug", "Issue #2", 5),
			new Issue("/issues/3", "/issuetypes/bug", "Issue #3", 2),
			new Issue("/issues/4", "/issuetypes/story", "Issue #4", 3),
			new Issue("/issues/5", "/issuetypes/story", "Issue #5", 1) };

	public IssueResource() {

	}

	@GET
	@Path("/{id}")
	@Timed
	public Response getStories(@PathParam("id") int id) {
		if (id > 0 && id <= issues.length) {
			return Response.ok(issues[id - 1]).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
